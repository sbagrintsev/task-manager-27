package ru.tsc.bagrintsev.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.enumerated.Sort;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.model.Project;
import ru.tsc.bagrintsev.tm.util.TerminalUtil;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

public class ProjectListCommand extends AbstractProjectCommand {

    @Override
    public void execute() throws IOException, AbstractException {
        System.out.println("[PROJECT LIST]");
        System.out.println("ENTER SORT TYPE: ");
        System.out.println(Arrays.toString(Sort.values()));
        @Nullable final String sortValue = TerminalUtil.nextLine();
        @Nullable final String userId = getUserId();
        @Nullable final Sort sort = Sort.toSort(sortValue);
        if (sort == null) {
            System.out.println("Sort type is wrong, applied default sort");
        }
        @Nullable final List<Project> projects = getProjectService().findAll(userId, sort);
        projects.forEach(System.out::println);
    }

    @NotNull
    @Override
    public String getName() {
        return "project-list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print project list.";
    }

}
