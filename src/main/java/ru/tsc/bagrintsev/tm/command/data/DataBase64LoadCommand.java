package ru.tsc.bagrintsev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.GeneralSecurityException;
import java.util.Base64;

public class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    public static final String NAME = "data-load-base64";

    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException, ClassNotFoundException {
        showOperationInfo();
        @NotNull final String base64 = new String(Files.readAllBytes(Paths.get(FILE_BASE64)));
        @NotNull final byte[] bytes = Base64.getDecoder().decode(base64);
        try (@NotNull final ByteArrayInputStream bais = new ByteArrayInputStream(bytes);
             @NotNull final ObjectInputStream ois = new ObjectInputStream(bais))
        {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from base64 file";
    }

}
