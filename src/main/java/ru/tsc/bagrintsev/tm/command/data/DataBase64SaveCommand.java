package ru.tsc.bagrintsev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.io.*;
import java.security.GeneralSecurityException;
import java.util.Base64;

public class DataBase64SaveCommand extends AbstractDataCommand{

    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException {
        showOperationInfo();
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        try (@NotNull final ByteArrayOutputStream baos = new ByteArrayOutputStream();
             @NotNull final ObjectOutputStream oos = new ObjectOutputStream(baos);
             @NotNull final FileOutputStream fos = new FileOutputStream(file, false))
        {
            oos.writeObject(domain);
            @NotNull final byte[] bytes = baos.toByteArray();
            @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
            fos.write(base64.getBytes());
            fos.flush();
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-base64";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in base64 file";
    }

}
