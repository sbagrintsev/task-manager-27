package ru.tsc.bagrintsev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.security.GeneralSecurityException;

public final class DataBinarySaveCommand  extends AbstractDataCommand{

    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException {
        showOperationInfo();
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        try (@NotNull final ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream(file, false)))
        {
            oos.writeObject(domain);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return "data-save-bin";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Save data in binary file";
    }

}
