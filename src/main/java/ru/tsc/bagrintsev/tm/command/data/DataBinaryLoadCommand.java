package ru.tsc.bagrintsev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.dto.Domain;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.io.*;
import java.security.GeneralSecurityException;

public class DataBinaryLoadCommand extends AbstractDataCommand{

    @NotNull
    public static final String NAME = "data-load-bin";

    @Override
    public void execute() throws IOException, AbstractException, GeneralSecurityException, ClassNotFoundException {
        showOperationInfo();
        try (@NotNull final ObjectInputStream ois = new ObjectInputStream(new FileInputStream(FILE_BINARY)))
        {
            @NotNull final Domain domain = (Domain) ois.readObject();
            setDomain(domain);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Load data from binary file";
    }

}
