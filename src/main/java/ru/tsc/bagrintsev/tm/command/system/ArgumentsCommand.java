package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.util.Collection;

public class ArgumentsCommand extends AbstractSystemCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[CommandLine arguments]");
        @NotNull Collection<AbstractCommand> repository = getCommandService().getAvailableCommands();
        repository.stream()
                .filter(c -> !c.getShortName().isEmpty())
                .forEach(c -> System.out.printf("%-35s%s\n", c.getShortName(), c.getDescription()));
    }

    @NotNull
    @Override
    public String getName() {
        return "arguments";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-arg";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print command-line arguments.";
    }

}
