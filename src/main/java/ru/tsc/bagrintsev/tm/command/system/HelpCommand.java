package ru.tsc.bagrintsev.tm.command.system;

import org.jetbrains.annotations.NotNull;
import ru.tsc.bagrintsev.tm.command.AbstractCommand;
import ru.tsc.bagrintsev.tm.exception.AbstractException;

import java.util.Collection;

public class HelpCommand extends AbstractSystemCommand {

    @Override
    public void execute() throws AbstractException {
        System.out.println("[Supported commands]");
        System.out.println("[While Running                       | Command Line             ]");
        System.out.println("[------------------------------------|--------------------------]");
        @NotNull Collection<AbstractCommand> repository = getCommandService().getAvailableCommands();
        repository.forEach(System.out::println);
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getShortName() {
        return "-h";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Print application help.";
    }

}
