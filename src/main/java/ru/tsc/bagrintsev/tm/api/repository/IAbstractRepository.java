package ru.tsc.bagrintsev.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.bagrintsev.tm.exception.AbstractException;
import ru.tsc.bagrintsev.tm.exception.entity.AbstractEntityException;
import ru.tsc.bagrintsev.tm.exception.field.IncorrectIndexException;
import ru.tsc.bagrintsev.tm.model.AbstractModel;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IAbstractRepository<M extends AbstractModel> {

    @NotNull
    M add(@NotNull final M record) throws AbstractException;

    @NotNull
    Collection<M> add(@NotNull final Collection<M> records);

    @NotNull
    Collection<M> set(@NotNull final Collection<M> records);

    @NotNull
    List<M> findAll();

    @NotNull
    List<M> findAll(@NotNull final Comparator<M> comparator);

    @NotNull
    M findOneByIndex(@NotNull final Integer index) throws IncorrectIndexException;

    @NotNull
    M findOneById(@NotNull final String id) throws AbstractException;

    boolean existsById(@NotNull final String id) throws AbstractException;

    @NotNull
    M remove(@NotNull final M model) throws AbstractException;

    @NotNull
    M removeByIndex(@NotNull final Integer index) throws AbstractException;

    @NotNull
    M removeById(@NotNull final String id) throws AbstractException;

    int totalCount();

    void clear();

    void removeAll(@NotNull final Collection<M> collection);
}
